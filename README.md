# Dockerize PHP

Membuat docker image dari aplikasi php.

Perintah untuk build:

    docker build -t belajar-docker-php .

Perintah untuk menjalankan image

    docker run -p 9090:80 belajar-docker-php

## Referensi ##

* [Image Resmi PHP](https://hub.docker.com/_/php)
* [Membuat docker image untuk Laravel](https://www.honeybadger.io/blog/laravel-docker-php/)