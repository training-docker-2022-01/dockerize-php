FROM --platform=linux/amd64 php:7.4-cli
COPY ./src /opt
ENTRYPOINT ["php","-S 0.0.0.0:12345","-t","/opt"]